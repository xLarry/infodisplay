# Geku Display

## What is this?

Local display with infomation for geku residents.

## Overview

Project is deployed to heroku [geku-display.herokuapp.com](http://geku-display.herokuapp.com).
Deployment is done via CD on Gitlab

A raspberryPI in the local geku-network fetches the sauna data and push it to firebase.
This local instance is started via "datafetcher.js".
The datafetcher just redirects old URLs to the heroku deployment and send the sauna-data.
There is no auto-deploy on the raspberry pi atm because docker is kind if brocken on the device.

TODO: Get docker container running on PI and use the watchtower project to auto-update.

## How to run with nodejs

    npm i -g grunt
    npm i
    grunt
    npm start

## How to run with docker

    docker build -t geku-infodisplay
    docker run geku-infodisplay