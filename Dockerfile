FROM node:9 as builder

ENV HOME=/opt/app
WORKDIR $HOME

COPY package.json package-lock.json $HOME/
RUN npm install

COPY . .

RUN ./node_modules/grunt-cli/bin/grunt

###########

FROM node:9-slim

ENV HOME=/opt/app \
    TZ=Europe/Berlin
WORKDIR $HOME

COPY package.json package-lock.json $HOME/
COPY --from=builder $HOME/node_modules $HOME/node_modules
COPY --from=builder $HOME/frontend $HOME/frontend
COPY --from=builder $HOME/services $HOME/services
COPY --from=builder $HOME/index.js $HOME/index.js
COPY --from=builder $HOME/datafetcher.js $HOME/datafetcher.js

EXPOSE 3000
CMD npm start