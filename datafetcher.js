var express = require('express');
var bodyParser = require('body-parser');

var saunaService = require('./services/saunaService.js');
var firebaseService = require('./services/firebaseService.js');

var app = express();
var router = express.Router();
var server = require('http').Server(app);
var io = require('socket.io')(server);

// middleware to use for all requests
router.use(function (req, res, next) {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader("Access-Control-Allow-Methods", "PUT,GET,DELETE,POST");
  next();
});

router.get('/', function (req, res) {
  res.redirect(301, 'http://geku-display.herokuapp.com/');
});

router.get('/index.html', function (req, res) {
  res.redirect(301, 'http://geku-display.herokuapp.com/');
});

router.get('/admin.html', function (req, res) {
  res.redirect(301, 'http://geku-display.herokuapp.com/admin.html');
});

router.get('/admin', function (req, res) {
  res.redirect(301, 'http://geku-display.herokuapp.com/admin.html');
});

app.use(router);

var port = process.env.PORT || 3000;

server.listen(port, () => console.log('Example app listening on port ' + port + '!'));


function updateSauna() {
  //console.log("updateSauna()")
  saunaService.getTemp()
    .then(firebaseService.postTemperatureToFireBase)
    //.then(() => console.log("updateSauna()->postTemperatureToFireBase() : done"))
}

// Send sauna to frontend every 5 minutes
setInterval(updateSauna, 1 * 60 * 1000);
updateSauna();