var firebase = require('firebase');

function initFirebase() {
    // Initialize Firebase
    var config = {
        apiKey: "AIzaSyB8x-vzSvnXZpKYcDbuwGYoaI7ceAsNZ8c",
        authDomain: "geku-info-display.firebaseapp.com",
        databaseURL: "https://geku-info-display.firebaseio.com",
        projectId: "geku-info-display",
        storageBucket: "",
        messagingSenderId: "805279184381"
    };
    firebase.initializeApp(config);
    firebase.auth().signInAnonymously().catch(function (error) {
        console.log(error);
    });
}
initFirebase();

function postTemperatureToFireBase(temperature) {
    // A post entry.
    var postData = {
        temperature: temperature,
        updatedAt: new Date()
    };

    // Write the new post's data simultaneously in the posts list and the user's post list.
    var updates = {};
    updates['/sauna'] = postData;

    firebase.database().ref().update(updates);

    return temperature
}

module.exports = {
    postTemperatureToFireBase
};