// Require the module
var Forecast = require('forecast.io');

var forecast = new Forecast({ APIKey: (process.env.FORECAST_API || '8d0565fce8e537b1d048e23ebc87f00a' ) });

var options = {
  units: 'si',
  lang: 'de',
  exclude: 'minutely,hourly'
}

function getDetails() {
  return new Promise(function (resolve, reject) {
    forecast.get(51.459265, 7.013491, options, function (err, res, weather) {
      if (err) reject(err);
      resolve(weather);
    });
  });
}
module.exports = {
  forecast,
  getDetails
}