angular.module("GekuInfodisplay")
  .component('messageCreate', {
    templateUrl : 'js/components/message-create/message-create.component.html',
    controller: function (MessageService) {

      this.fileNameChanged = (fileInputElement) => {
        this.image = fileInputElement.files[0]
      }

      this.createMessage = (message, image) => {
        MessageService
          .create({content : message}, image)
          .then((res) => {
            window.location = '/';
          });
      };

    }
  });