angular.module("GekuInfodisplay")
  .component('message', {
    templateUrl: 'js/components/message/message.component.html',
    bindings: {
      message: '='
    },
    controller: function ($location, MessageService) {

      this.deleteMessage = (messageId) => {
        if (confirm("Diese Nachricht wirklich löschen?")) {
          MessageService.delete(messageId)
        }
      };

      this.$onInit = function () {
        this.displayMode = ($location.search().display == 1)
      };

      this.$postLink = ($element) => {
        var message = this.message;
        window.loadImage(message.message.imageUrl, function (img) {
          if (img.type === "error") {
            console.log("couldn't load image:", img);
          } else {
            window.EXIF.getData(img, function () {
                var orientation = EXIF.getTag(this, "Orientation");
                var canvas = window.loadImage.scale(img, {orientation: orientation || 0, canvas: true, maxWidth: $(".container-"+message.id).width()});
                $(".container-"+message.id).append(canvas);

            });
          }
        });
      }
    }
  });