angular.module("GekuInfodisplay")
  .component('messageList', {
    templateUrl : 'js/components/message-list/message-list.component.html',
    controller: function ($interval, MessageService) {

      this.deleteMessage = (messageId) => {
        if (confirm("Diese Nachricht wirklich löschen?")) {
          MessageService.delete(messageId)
        }
      };

      MessageService.events.subscribe(messages => this.messages = messages);
    }
  });