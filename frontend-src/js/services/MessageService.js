angular.module("GekuInfodisplay")

  .factory("MessageService", function MessageService(rx) {

    function createMessage(message, image) {
      return uploadImage(image)
        .then((result) => {
          if (result) {
            message.imageUrl = result.downloadURL
          }
          return message
        })
        .then(postMessage)
    }

    function postMessage(message) {
      // A post entry.
      var postData = {
        message: message,
        createdAt: new Date()
      };

      // Get a key for a new Post.
      var newPostKey = firebase.database().ref().child('posts').push().key;

      // Write the new post's data simultaneously in the posts list and the user's post list.
      var updates = {};
      updates['/posts/' + newPostKey] = postData;

      return firebase.database().ref().update(updates);
    }

    function deleteMessage(messageId) {
      var updates = {};
      updates['/posts/' + messageId] = null;

      return firebase.database().ref().update(updates);
    }

    function uploadImage(image) {
      if (image) {
        var storageRef = firebase.storage().ref();
        return storageRef.child('images/'+ Date.now() + "-" + image.name).put(image);
      }
      return new Promise((resolve) => resolve(null))
    }

    function processMessages(snapshot) {
      var messages = [];
      for (const property in snapshot) {
        if (snapshot.hasOwnProperty(property)) {
          let message = snapshot[property];
          message.id = property;
          messages.push(message);
        }
      };
      return messages
    }

    function createSubjectForMessages() {
      var subject = new rx.Subject();

      firebase
        .database()
        .ref('posts/')
        .limitToLast(5)
        .on('value', (snapshot) => {
          let messages = processMessages(snapshot.val());
          subject.onNext(messages)
        });

      return subject;
    }


    return {
      create: createMessage,
      delete: deleteMessage,
      events: createSubjectForMessages()
    };
  });

