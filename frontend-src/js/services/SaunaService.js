angular.module("GekuInfodisplay")

  .factory("SaunaService", SaunaService);

function SaunaService($http, socket, rx) {

  var subject = new rx.Subject();

  firebase.database().ref("sauna").child("temperature").on("value", function (snapshot) {
    //console.log(snapshot.val());
    subject.onNext(snapshot.val())
  });

  return {
    get: function () {
      return $http.get('/api/sauna')
        .then(response => response.data)
    },
    events: subject
  };
}