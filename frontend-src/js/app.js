angular.module("GekuInfodisplay", ['ngResource', 'rx', 'btford.socket-io'])
  .run(function () {
    moment.locale('de');
  })
  .factory('socket', socketFactory => socketFactory());

